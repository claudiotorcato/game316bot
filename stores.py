from datetime import datetime

from pony.orm import db_session, commit, delete, set_sql_debug
from models import Campanha, Personagem, Item, Planeta, Missao, Encontro
from constants import bool_values


class ObjectNotFound(Exception):
    pass


@db_session
def pegar_planeta(categoria, numero):
    return Planeta.get(
        categoria=categoria,
        numero=numero)

@db_session
def listar_planetas(categoria):
    return Planeta.select(lambda p: p.categoria == categoria)[:]


@db_session
def criar_campanha(nome, grupo, criador):
    Campanha(nome=nome, grupo=grupo, criador=criador,data_criacao=datetime.now())
    commit()


@db_session
def criar_missao(
        nome_planeta,
        description_planet,
        aa,
        criatura,
        habilidade_especial,
        narrador_id,
        campanha_id):
    campanha = Campanha.get(id=campanha_id)
    total_marcadores = campanha.total_jogadores * 5
    # TODO devemos verificar o fator de multiplicação dos marcadores de ameaça
    # pois esse fator deixa de ser 5 com o avanço da campanha
    missao = Missao(
        planeta=nome_planeta, descricao_planeta=description_planet,
        alien_ability=aa, criatura=criatura,
        habilidade_especial=habilidade_especial,
        marcadores=total_marcadores,
        marcadores_restantes=total_marcadores,
        narrador=narrador_id,
        campanha=campanha)
    commit()
    return missao


@db_session
def procurar_missao_ativa(campanha_id):
    campanha = Campanha.get(id=campanha_id)
    return Missao.get(campanha=campanha, ativa=True)


@db_session
def fechar_missao(id_missao):
    missao = Missao.get(id=id_missao)
    missao.ativa = False
    commit()


@db_session
def salvar_categoria_planeta(campanha, categoria_planeta):
    c = Campanha.get(nome=campanha.nome, grupo=str(campanha.grupo))
    if c:
        c.categoria_planeta = categoria_planeta
    commit()

@db_session
def salvar_descricao_planeta(missao, descricao_planeta):
    missao = Missao.get(id=missao.id)
    if missao:
        missao.descricao_planeta = descricao_planeta
    commit()

@db_session
def salvar_forma_criatura(missao, forma_criatura):
    missao = Missao.get(id=missao.id)
    if missao:
        missao.criatura = forma_criatura
    commit()    

@db_session
def salvar_habilidade_criatura(missao, habilidade_especial):
    missao = Missao.get(id=missao.id)
    if missao:
        missao.habilidade_especial = habilidade_especial
    commit()        

@db_session
def salvar_nome_planeta(missao, nome_planeta):
    missao = Missao.get(id=missao.id)
    if missao:
        missao.planeta = nome_planeta
    commit()        


@db_session
def salvar_ha(missao, valor):
    missao = Missao.get(id=missao.id)
    if int(valor) < 1 or int(valor) > 10:
        raise Exception('Incorrect value. Values between 1 and 10.')
    if missao:
        missao.alien_ability = int(valor)
    commit()            

@db_session
def procurar_campanha_ativa(grupo):
    return Campanha.get(grupo=grupo, ativa=True)


@db_session
def desativar_campanha(grupo):
    campanha = Campanha.get(grupo=grupo)
    campanha.ativa = False
    commit()


@db_session
def reativar_campanha(grupo):
    campanha = Campanha.get(grupo=grupo)
    campanha.ativa = True
    commit()


@db_session
def criar_personagem(usuario, nome, grupo):
    print(grupo)
    campanha = Campanha.get(grupo=grupo, ativa=True)
    if not campanha:
        return
    p = Personagem(
        campanha=campanha,
        nome=nome, usuario=usuario, hp=3, reputation='',
        especialization='', kills=0, totalKills=0,
        rank='0')
    commit()
    return p


@db_session
def apagar_pjs_campanha(nome_campanha):
    campanha = Campanha.get(nome=nome_campanha)
    if not campanha:
        return
    delete(p for p in Personagem if p.campanha == campanha)
    commit()


@db_session
def procurar_personagem(id_grupo, nome_usuario):
    campanha = Campanha.get(grupo=id_grupo, ativa=True)
    if not campanha:
        return None
    return Personagem.get(usuario=nome_usuario, campanha=campanha, ativo=True)


@db_session
def mudar_atributo(id_grupo, nome_usuario, atributo, valor):
    if atributo == 'name':
        atributo = 'nome'
    if atributo == 'specialization':
        atributo = 'especialization'
    campanha = Campanha.get(grupo=id_grupo, ativa=True)
    if not campanha:
        return None
    personagem = Personagem.get(usuario=nome_usuario, campanha=campanha)
    if not personagem:
        return None
    if atributo in ['armor', 'drugs']:
        valor = bool_values[valor]
    personagem.set(**{atributo: valor})
    commit()


@db_session
def somar_mortes(grupo, usuario, incremento):
    set_sql_debug()
    campanha = Campanha.get(grupo=grupo, ativa=True)
    if not campanha:
        return None
    personagem = Personagem.get(usuario=usuario, campanha=campanha, ativo=True)
    if not personagem:
        return None
    mortes = personagem.kills
    personagem.set(kills=mortes + incremento)
    commit()


@db_session
def save_hp_character(id, hp):
    character = Personagem[id]
    character.hp = hp
    commit()


@db_session
def procurar_personagens(id_grupo):
    campanha = Campanha.get(grupo=id_grupo, ativa=True)
    if not campanha:
        return []
    return Personagem.select(lambda p: p.campanha.id == campanha.id and p.ativo == True)[:]

@db_session
def arquivar_personagem(pj):
    p = Personagem.get(id=pj.id)
    p.ativo = False
    commit()


@db_session
def novo_item(grupo, usuario, nome, proxima="-", media="-", longa="-"):
    campanha = Campanha.get(grupo=grupo, ativa=True)
    if not campanha:
        print('campanha não encontrada')
        return None
    personagem = Personagem.get(usuario=usuario, campanha=campanha)
    if not personagem:
        print('PJ não encontrado')
        return None
    item = Item(
        personagem=personagem, nome=nome,
        proxima=proxima, media=media, longa=longa
    )
    commit()
    return item


@db_session
def procurar_itens(personagem):
    return Item.select(lambda i: i.personagem.id == personagem.id)[:]


@db_session
def buscar_atributos(id_grupo):
    campanha = Campanha.get(grupo=id_grupo, ativa=True)
    if not campanha:
        return []
    lista = Personagem.select(lambda p: p.campanha.id == campanha.id)[:]
    fas = nfas = []
    for p in lista:
        fas.append(p.fa)
        nfas.append(p.nfa)
    return fas, nfas

@db_session
def procurar_encontro_ativo(missao):
    return Encontro.get(missao=missao.id, ativo=True)

@db_session
def criar_encontro(id_grupo, marcadores):
    campanha = procurar_campanha_ativa(id_grupo)
    missao = procurar_missao_ativa(campanha.id)
    encontro = Encontro(missao=missao, marcadores=marcadores)
    ameaca_planeta = missao.marcadores_restantes
    ameaca_planeta = ameaca_planeta - marcadores
    missao.marcadores_restantes = ameaca_planeta
    commit()
    return encontro

@db_session
def procurar_campanhas_ativas():
    return Campanha.select()[:]