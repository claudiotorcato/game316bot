import math

import constants


def detail(stat, character):
    description = """
{stat}: {value}

Possible options: {options}
    """
    result = ''
    if stat == 'name':
        result = description.format(**{
            'stat': stat.capitalize(),
            'value': eval('character.nome'),
            'options': constants.values_possible[stat]
        })
    if stat == 'specialization':
        result = description.format(**{
            'stat': stat.capitalize(),
            'value': eval('character.especialization'),
            'options': constants.values_possible[stat]
        })
    if stat in [
        'fa',
        'nfa',
        'kills',
        'totalKills',
        'hp',
        'rank'
    ]:
        result = description.format(**{
            'stat': stat.capitalize(),
            'value': eval('character.{stat}'.format(stat=stat)),
            'options': constants.values_possible[stat]
        })
    if stat in ['armor', 'drugs']:
        result = description.format(**{
            'stat': stat.capitalize(),
            'value': constants.used_values[eval('character.{stat}'.format(
                stat=stat))],
            'options': constants.values_possible[stat]
        })
    return result


def validate_value(stat, value):
    if stat == 'drugs':
        if value in constants.drugs_valids:
            return True
        return False
    if stat == 'armor':
        if value in constants.armor_valids:
            return True
        return False
    return True


def define_alien_ability(fas, nfas, opcao):
    opcoes = [
        sorted(fas)[0],
        sorted(fas)[0] + 1,
        sorted(fas)[0] + 2,
        sorted(nfas)[0],
        sorted(nfas)[0] + 1,
        sorted(nfas)[0] + 2,
        sorted(fas, reverse=True)[0],
        sorted(fas, reverse=True)[0] - 1,
        sorted(fas, reverse=True)[0] - 2,
        sorted(nfas, reverse=True)[0],
        sorted(nfas, reverse=True)[0] - 1,
        sorted(nfas, reverse=True)[0] - 2,
        math.ceil(sorted(fas, reverse=True)[0] + sorted(nfas)[0] / 2),
        math.ceil(sorted(fas, reverse=True)[0] + sorted(nfas)[0] / 2),
        math.ceil(sorted(fas, reverse=True)[0] + sorted(nfas)[0] / 2),
        math.ceil(sorted(nfas, reverse=True)[0] + sorted(fas)[0] / 2),
        math.ceil(sorted(nfas, reverse=True)[0] + sorted(fas)[0] / 2),
        math.ceil(sorted(nfas, reverse=True)[0] + sorted(fas)[0] / 2),
        5,
        10
    ]
    print('Opção: ' + str(opcao))
    return opcoes[opcao]
