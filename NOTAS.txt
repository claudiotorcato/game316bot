createchar - Create a new character (ex.: /createchar [ @user ] character_name
creategame - Create a new game
damage - increase damage on trooper (ex.: /damage [ @user ] )
kills - Total mission deaths
listitems - List items. (ex.: /listitems [ @user ] )
newitem - trooper gets a item (ex.: /newitem Brush)
stats - Show character's stats (ex.: /stats [ @user ] )
stat - Update stat (ex.: /stat [ @user ] stat new_value)

comandos Heroku
------------------

git push heroku master

heroku ps:scale web=1
heroku ps:scale worker=1

heroku local

heroku logs --tail
