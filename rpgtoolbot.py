import botogram
import os

TOKEN = os.environ['TOKEN_RPGTOOLBOT']

bot = botogram.create(TOKEN)
bot.about = 'Several tools tabletop RPG'
bot.owner = '@Cleedee'
bot.lang = 'br'

@bot.command('hello')
def hello_command(chat, message, args):
    chat.send('Hello World')