from pony.orm import db_session, commit, delete, set_sql_debug
from models import Campanha, Personagem, Item, Missao, Planeta


@db_session
def importar_planetas():
    set_sql_debug()
    Planeta(numero=1, nome='Bosch', categoria='pintores')
    Planeta(numero=2, nome='Caravaggio', categoria='pintores')
    Planeta(numero=3, nome='Cézanne', categoria='pintores')
    Planeta(numero=4, nome='Degas', categoria='pintores')
    Planeta(numero=5, nome='Durer', categoria='pintores')
    Planeta(numero=6, nome='Goya', categoria='pintores')
    Planeta(numero=7, nome='Holbein', categoria='pintores')
    Planeta(numero=8, nome='Kandinsky', categoria='pintores')
    Planeta(numero=9, nome='Matisse', categoria='pintores')
    Planeta(numero=10, nome='Michelangelo', categoria='pintores')
    Planeta(numero=11, nome='Monet', categoria='pintores')
    Planeta(numero=12, nome='Picasso', categoria='pintores')
    Planeta(numero=13, nome='Pollock', categoria='pintores')
    Planeta(numero=14, nome='Portinari', categoria='pintores')
    Planeta(numero=15, nome='Rembrandt', categoria='pintores')
    Planeta(numero=16, nome='Renoir', categoria='pintores')
    Planeta(numero=17, nome='Reubens', categoria='pintores')
    Planeta(numero=18, nome='Titian', categoria='pintores')
    Planeta(numero=19, nome='Warhol', categoria='pintores')
    Planeta(numero=20, nome='Whistler', categoria='pintores')
    Planeta(numero=1, nome='Anderson', categoria='marxistas')
    Planeta(numero=2, nome='Bakhtin', categoria='marxistas')
    Planeta(numero=3, nome='Gramsci', categoria='marxistas')
    Planeta(numero=4, nome='Hobsbawn', categoria='marxistas')
    Planeta(numero=5, nome='Lenin', categoria='marxistas')
    Planeta(numero=6, nome='Lukacs', categoria='marxistas')
    Planeta(numero=7, nome='Luxemburgo', categoria='marxistas')
    Planeta(numero=8, nome='Marx', categoria='marxistas')
    Planeta(numero=9, nome='Engels', categoria='marxistas')
    Planeta(numero=10, nome='Trotsky', categoria='marxistas')
    Planeta(numero=11, nome='Zizek', categoria='marxistas')
    Planeta(numero=12, nome='Chaui', categoria='marxistas')
    Planeta(numero=13, nome='Horkheimer', categoria='marxistas')
    Planeta(numero=14, nome='Lefevbre', categoria='marxistas')
    Planeta(numero=15, nome='Meszaros', categoria='marxistas')
    Planeta(numero=16, nome='Losurdo', categoria='marxistas')
    Planeta(numero=17, nome='Barata-Moura', categoria='marxistas')
    Planeta(numero=18, nome='Pachukanis', categoria='marxistas')
    Planeta(numero=19, nome='Callinicos', categoria='marxistas')
    Planeta(numero=20, nome='Colletti', categoria='marxistas')
    commit()

if __name__ == '__main__':
    importar_planetas()