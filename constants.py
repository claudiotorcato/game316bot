hpLabels = ['Dead', 'Crippled', 'Messed', 'OK']


atributos = [
    'name', 'rank', 'specialization', 'reputation',
    'fa', 'nfa', 'hp', 'armor', 'drugs', 'kills', 'totalKills'
]

rankLabels = [
    'Trooper', 'Corporal', 'Sergeant', 'Lieutenant',
    'Captain', 'Major', 'LT Colonel', 'Colonel', 'Brigadier'
]

values_possible = {
    'hp': """
0 - Dead
1 - Cripped
2 - Messed
3 - OK
    """,
    'rank': """
0 - Trooper
1 - Corporal
2 - Sergeant
3 - Lieutenant
4 - Captain
5 - Major
6 - LT Colonel
7 - Colonel
8 - Brigadier
    """,
    'specialization': """
Medic, Armorer, Scout, Grenadier, Radio Operator and Sapper
    """,
    'armor': 'Used ou Not Used',
    'drugs': 'Used ou Not Used',
    'fa': '2 - 10',
    'nfa': '2 - 10',
    'kills': '0+',
    'totalKills': '0+',
    'reputation': 'Free',
    'name': 'Free',
}

hp_valids = range(4)

rank_valids = range(9)

especialization_valids = [
    'Medic', 'Armore', 'Scout', 'Grenadier',
    'Radio Operator', 'Sapper'
]

armor_valids = ['Used', 'Not Used']

drugs_valids = ['Used', 'Not Used']

used_values = {True: 'Used', False: 'Not Used'}

bool_values = {'Used': True, 'Not Used': False}

description_planets = [
    'Arid world', 'Asteroid belt', 'Cloud/gas planet', 'Dense atmosphere',
    'Desert world', 'Electrical storms', 'Forested surface', 'High gravity',
    'High humidity', 'Ice covered', 'Low gravity', 'Mountainous terrain',
    'Pleasure planet', 'Poisonous atmosphere', 'Radioactive',
    'Rain world', 'Reefs and coral islands', 'Temperate',
    'Volcanically active', 'Water world'
]

creature_form = [
    'Advanced Humanoids', 'Apes', 'Artificial lifeforms',
    'Birds or Flying Beasts', 'Corrupt Troopers', 'Dinosaurs', 'Dogs',
    'Felines', 'Furred Creatures', 'Giants', 'Humanoids', 'Insectoids',
    'Mineral-based forms', 'Oozes', 'Plants', 'Rays, sharks or fish',
    'Reptiles or Amphibians', 'Sentient planet', 'Shadow Beasts', 'Sirens'
]

special_ability = [
    'Ambush', 'Armour', 'Boost Ability', 'End Encounter', 'Enrage',
    'Exploding Bodies', 'Flee', 'Ignore Armour', 'Ignore Wounds', 'Impair',
    'Induce Weakness', 'Isolate', 'Lasting Wounds', 'Leaping',
    'Rapid Movement', 'Reduce Visibility', 'Regeneration', 'Stop Technology',
    'Suicide', 'Swarm'
]
