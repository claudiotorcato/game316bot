from pony.orm import (Database, Required, Set, Optional, set_sql_debug)
import os
import urllib.parse
from datetime import datetime


db = Database()


class Campanha(db.Entity):
    nome = Required(str)
    grupo = Required(str)
    criador = Required(int)
    ativa = Required(bool, default=True)
    data_criacao = Optional(datetime)
    data_conclusao = Optional(datetime)
    personagens = Set('Personagem')
    missoes = Set('Missao')
    planetas = Set('Planeta')
    categoria_planeta = Optional(str, default='pintores')

    @property
    def total_jogadores(self):
        return len(list(filter(lambda p: p.ativo, self.personagens)))


class Planeta(db.Entity):
    numero = Required(int)
    nome = Required(str)
    categoria = Required(str)
    campanhas = Set(Campanha)


class Missao(db.Entity):
    planeta = Optional(str)
    descricao_planeta = Optional(str)
    alien_ability = Optional(int)
    criatura = Optional(str)
    habilidade_especial = Optional(str)
    narrador = Required(int)
    ativa = Required(bool, default=True)
    marcadores = Required(int)
    marcadores_restantes = Required(int)
    campanha = Required(Campanha)
    encontros = Set('Encontro')

    @property
    def total_encontros(self):
        return len(self.encontros)    

class Encontro(db.Entity):
    missao = Required(Missao)
    marcadores = Required(int)
    ativo = Required(bool, default=True)


class Personagem(db.Entity):
    campanha = Required(Campanha)
    nome = Required(str)
    usuario = Required(str)
    hp = Required(int)
    reputation = Optional(str)
    especialization = Optional(str)
    kills = Required(int)
    totalKills = Required(int)
    rank = Required(str)
    fa = Required(int, default=0)
    nfa = Required(int, default=0)
    armor = Required(bool, default=False)
    drugs = Required(bool, default=False)
    itens = Set('Item')
    ativo = Required(bool, default=True)


class Item(db.Entity):
    personagem = Required(Personagem)
    nome = Required(str)
    proxima = Optional(str)
    media = Optional(str)
    longa = Optional(str)


if 'postgres' in os.environ["DATABASE_URL"]:
    urllib.parse.uses_netloc.append("postgres")
    url = urllib.parse.urlparse(os.environ["DATABASE_URL"])
    db.bind(
        'postgres',
        user=url.username,
        password=url.password,
        host=url.hostname,
        database=url.path[1:])
    print(url)
else:
    set_sql_debug(True, show_values=True)
    db.bind('sqlite', 'database.sqlite', create_db=True)
db.generate_mapping(create_tables=True)
