
import botogram
import stores
import details
import constants
import sys
import os
import random
import string
from datetime import datetime
from pony.orm import db_session, commit

# inspirado no seguinte codigo:
# https://github.com/arnoid/slack-bot-carnage-among-the-stars

TOKEN = os.environ['TOKEN_GAME316BOT']

bot = botogram.create(TOKEN)
bot.about = 'This bot is a tool to play 3:16 Carnage in the Stars RPG'
bot.owner = '@Cleedee'
bot.lang = 'br'

bot.after_help = ['Para ajuda, acesse: <a href="https://telegra.ph/Manual-do-bot-Game316Bot-11-16">Manual do Game316Bot</a> ', ]


def retirar_nome_bot(texto):
    return texto.replace('@' + bot.itself.username, '')


@bot.chat_unavailable
def inativar_campanha(chat_id, reason):
    campanha = stores.procurar_campanha_ativa(str(chat_id))
    if campanha:
        stores.desativar_campanha(str(chat_id))


@bot.command('creategame')
def create_campaign(chat, message, args):
    """Create a campaign in chat. (Eg.: /creategame name_campaign)"""
    texto = retirar_nome_bot(message.text)
    nome_campanha = " ".join(texto.split()[1:])
    if not nome_campanha:
        chat.send('Create a campaign in chat. \
            (Eg.: /creategame name_campaign)')
        # TODO autoreferencia a stringdoc
        return
    if chat.type not in ('group', 'supergroup'):
        chat.send('You can not create a campaign outside a group.')
        return
    if not stores.procurar_campanha_ativa(str(chat.id)):        
        stores.criar_campanha(nome=nome_campanha, grupo=str(chat.id), criador=message.sender.id)
        chat.send('Campaign ' + nome_campanha + ' created.')
    else:
        chat.send('the active campaign already exists in this group.')

@bot.command('closegame')
@db_session
def close_game(chat, message, args):
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return
    campanha.ativa = False
    campanha.data_conclusao = datetime.now()
    commit()    
    chat.send('Campanha fechada.')    

@bot.command('startmission')
def create_mission(chat, message, args):
    """Create a mission. (Eg. /startmission)"""
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha.categoria_planeta:
        chat.send(
            "Set the category of the planets for this campaign with "
            + " command /setplanets.")
        return
    numero = random.randint(1, 20)
    planeta = stores.pegar_planeta(campanha.categoria_planeta, numero)
    descricao_basica = constants.description_planets[random.randint(0, 19)]
    criatura = constants.creature_form[random.randint(0, 19)]
    habilidade_especial = constants.special_ability[random.randint(0, 19)]
    fas, nfas = stores.buscar_atributos(str(chat.id))
    aa = details.define_alien_ability(fas, nfas, random.randint(1, 20) - 1)
    missao = stores.procurar_missao_ativa(campanha.id)
    if missao:
        chat.send('There is already an active mission.')
        return
    missao = stores.criar_missao(
        planeta.nome, descricao_basica, aa, criatura, habilidade_especial,
        message.sender.id,
        campanha.id
    )
    chat.send('Mission created.')
    chat.send('Planet name: ' + missao.planeta)
    chat.send('Alien Ability: ' + str(missao.alien_ability))
    chat.send('Basic Creature Form: ' + str(missao.criatura))
    chat.send('Special Ability: ' + str(missao.habilidade_especial))
    chat.send('Basic Planet Description: ' + missao.descricao_planeta)


@bot.command('finishmission')
def terminar_missao(chat, message, args):
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return
    missao = stores.procurar_missao_ativa(campanha.id)
    stores.fechar_missao(missao.id)
    chat.send('Mission closed.')

@bot.command('currentmission')
@db_session
def mostrar_missao_atual(chat, message, args):
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    missao = stores.procurar_missao_ativa(campanha.id)
    if not missao:
        chat.send('Mission not found.')
        return
    msg = """
Mission Information
----------------------------
Planet: {}
Basic Planet Description: {}
Threat Tokens: {}
Current Threat Tokens: {}
Encounters: {}
Alien Ability: {}
Basic Criature Form: {}
Special Ability: {}
    """
    chat.send(
        msg.format(
            missao.planeta,
            missao.descricao_planeta,
            missao.marcadores,
            missao.marcadores_restantes,
            missao.total_encontros,
            missao.alien_ability,
            missao.criatura,
            missao.habilidade_especial)
    )

@bot.command('setplanets')
def mudar_planetas(chat, message, args):
    btns = botogram.Buttons()
    btns[0].callback('Pintors', 'changeplanets', 'pintores')
    btns[1].callback('Marxists', 'changeplanets', 'marxistas')

    chat.send("What category of planet for your campaign? ", attach=btns)


@bot.callback('changeplanets')
def mudar_categoria_planeta(query, data, chat, message):
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return
    stores.salvar_categoria_planeta(campanha, str(data))
    message.delete()
    chat.send('Thanks for the info.')


# metodos para alterar dados da missão

@bot.callback('setdescplanets')
def mudar_descricoes_planetas(query, chat, message):
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return        
    btns = botogram.Buttons()
    for i, desc in enumerate(constants.description_planets):
        btns[i].callback(desc, 'changedescplanets', desc)
    btns[i + 1].callback('Sair', 'sair')        
    message.delete()
    missao = stores.procurar_missao_ativa(campanha.id)        
    chat.send("What basic description planet for your campaign? (currently {})".format(missao.descricao_planeta), attach=btns)

@bot.callback('changedescplanets')
def mudar_descricao_planeta(query, data, chat, message):
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return
    missao = stores.procurar_missao_ativa(campanha.id)        
    stores.salvar_descricao_planeta(missao, str(data))
    message.delete()
    query.notify('Thanks for the info.')

@bot.callback('setcreatures')
def listar_criaturas(query, chat, message):
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return    
    btns = botogram.Buttons()
    for i, desc in enumerate(constants.creature_form):
        btns[i].callback(desc, 'changecreature', desc)
    btns[i + 1].callback('Sair', 'sair')        
    message.delete()
    missao = stores.procurar_missao_ativa(campanha.id)
    chat.send("What form creature for your campaign? (currently {})".format(missao.criatura), attach=btns)

@bot.callback('changecreature')
def mudar_forma_criatura(query, data, chat, message):
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return
    missao = stores.procurar_missao_ativa(campanha.id)        
    stores.salvar_forma_criatura(missao, str(data))
    message.delete()
    query.notify('Thanks for the info.')

@bot.callback('setabilities')
def listar_habilidades(query, chat, message):
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return    
    btns = botogram.Buttons()
    for i, desc in enumerate(constants.special_ability):
        btns[i].callback(desc, 'changeability', desc)
    btns[i + 1].callback('Sair', 'sair')
    message.delete()
    missao = stores.procurar_missao_ativa(campanha.id)
    chat.send("What creature's special ability for your campaign? (currently {})".format(missao.habilidade_especial), attach=btns)

@bot.callback('changeability')
def mudar_habilidade_criatura(query, data, chat, message):
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return
    missao = stores.procurar_missao_ativa(campanha.id)        
    stores.salvar_habilidade_criatura(missao, str(data))
    message.delete()
    query.notify('Thanks for the info.')

@bot.callback('setplanet')
def listar_planetas(query, chat, message):
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return
    planetas = stores.listar_planetas(campanha.categoria_planeta)
    btns = botogram.Buttons()
    for i, planeta in enumerate(planetas):
        btns[i].callback(planeta.nome, 'changeplanet', planeta.nome)
    btns[i + 1].callback('Sair', 'sair')
    message.delete()
    missao = stores.procurar_missao_ativa(campanha.id)
    chat.send("Select planet (currently {}) ".format(missao.planeta), attach=btns)    

@bot.callback('changeplanet')
def mudar_planeta(query, data, chat, message):
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return
    missao = stores.procurar_missao_ativa(campanha.id)        
    stores.salvar_nome_planeta(missao, str(data))
    message.delete()
    query.notify('Thanks for the info.')

@bot.callback('setha')
def listar_ha(query, chat, message):
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return
    btns = botogram.Buttons()
    btns[0].callback('2', 'changeha', '2')
    btns[0].callback('3', 'changeha', '3')
    btns[0].callback('4', 'changeha', '4')
    btns[1].callback('5', 'changeha', '5')
    btns[1].callback('6', 'changeha', '6')
    btns[1].callback('7', 'changeha', '7')
    btns[2].callback('8', 'changeha', '8')
    btns[2].callback('9', 'changeha', '9')
    btns[2].callback('10', 'changeha', '10')
    btns[3].callback('Sair', 'sair')
    message.delete()
    missao = stores.procurar_missao_ativa(campanha.id)
    chat.send("Select AA (currently {}) ".format(missao.alien_ability), attach=btns)    

@bot.callback('changeha')
def mudar_ha(query, data, chat, message):
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return
    missao = stores.procurar_missao_ativa(campanha.id)        
    stores.salvar_ha(missao, str(data))
    message.delete()
    query.notify('Thanks for the info.')


@bot.command('changemission')
def listar_opcoes_missao(chat, message, args):
    # TODO
    btns = botogram.Buttons()
    btns[0].callback('List of Planets', 'setplanet')
    btns[0].callback('Basic Planet Description', 'setdescplanets')
    btns[1].callback('Basic Creature Form', 'setcreatures')
    btns[1].callback('Special Ability', 'setabilities')
    btns[2].callback('AA', 'setha')
    btns[3].callback('Sair', 'sair')

    chat.send("O que você deseja alterar? ", attach=btns)    

@bot.callback('sair')
def sair_das_opcoes_de_botao(query, chat, message):
    message.delete()
    query.notify('Thanks for the info.')

# FIM dos metodos para alterar dados da missão

@bot.command('defineranks')
def definir_patentes(chat, message, args):
    chat.send(
        'Esse comando atribuirá patentes aos PJs seguindo as regras do jogo.'
    )

@bot.command('statusgame')
@db_session
def status_game(chat, message, args):
    # TODO como obter nick do usuário criador da campanha?
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return
    if not campanha.criador:
        campanha.criador = message.sender.id
        commit()
    msg = '''
Campaign Information
----------------------------
Name: {}
Status: {}
Taxonomy Planet: {}
Players: {}
    '''
    chat.send(msg.format(
        campanha.nome,
        'Ativo ' if campanha.ativa else 'Inativo',
        campanha.categoria_planeta,
        campanha.total_jogadores
    ))

@bot.command('newencounter')
def novo_encontro(chat, message, args):
    """Create a new encounter and actives some commands.
    (Ex.: /newencounter tokens)"""
    if args and args[0].isdigit():
        campanha = stores.procurar_campanha_ativa(str(chat.id))
        missao = stores.procurar_missao_ativa(campanha.id)
        if stores.procurar_encontro_ativo(missao):
            chat.send('Há um encontro ativo. Encerre o anterior.')
            return
        stores.criar_encontro(str(chat.id), int(args[0]))
        chat.send('Esse comando iniciará um encontro.')    
    else:
        chat.send('Marcadores de ameaça inválido ou não informado.')

@bot.command('finishencounter')
def terminar_encontro(chat, message, args):
    pass

@bot.command('special')
def executar_habilidade_especial(chat, message, args):
    chat.send('Esse comando executará a habilidade especial dos alienígenas.')
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return
    missao = stores.procurar_missao_ativa(campanha.id)
    chat.send(missao.habilidade_especial)    

@bot.command('tostand')
def entrar_na_missao(chat, message, args):
    chat.send('Esse comando fará o jogador participar da missão')

@bot.command('players')
def listar_jogadores(chat, message, args):
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return
    pjs = stores.procurar_personagens(campanha.grupo)
    lista = ['Players Campaign', '----------------------------']
    for pj in pjs:
        lista.append('{usuario} ({nome})'.format(usuario=pj.usuario, nome=pj.nome))
    chat.send('\n'.join(lista))        

@bot.command('createchar')
def create_character(chat, message, args):
    """Create a character linked to user in active campaign.
    (Ex.: /createchar [ @user ] character_name)"""
    if args:
        if '@' in args[0]:
            nome_usuario = args[0].replace('@','')
        else:
            nome_usuario = message.sender.username
    else:
        chat.send('Syntax error: /createchar [ @user ] character_name')
        return
    nome_personagem = " ".join(args[1 if '@' in args[0] else 0:])
    if not nome_personagem:
        chat.send('Syntax error: /createchar [ @user ] character_name')
        return
    p = stores.procurar_personagem(str(chat.id), nome_usuario)
    if p:
        chat.send('User have active PC.')
        return
    p = stores.criar_personagem(nome_usuario, nome_personagem, str(chat.id))
    if not p:
        chat.send('Campaign not found.')
        return
    chat.send('Character ' + nome_personagem + ' created.')


@bot.command('archivechar')
def arquivar_personagem(chat, message, args):
    # TODO O criador do jogo pode arquivar qualquer PJ indicando o jogador no comando
    campanha = stores.procurar_campanha_ativa(str(chat.id))
    if not campanha:
        chat.send('Campaign not found.')
        return
    if campanha.criador == message.sender.id and botogram.usernames_in(message.text):
        # criador da campanha arquiva o PJ de um jogador
        nome_usuario = botogram.usernames_in(message.text)[0]
    else:
        nome_usuario = message.sender.username
    pj = stores.procurar_personagem(str(chat.id), nome_usuario)
    if pj:
        stores.arquivar_personagem(pj)
        chat.send('Personagem arquivado com sucesso.')
        return
    else:
        chat.send('Não existe Personagem ativo.')

@bot.command('reactivate')
def reativar(chat, message, args):
    stores.reativar_campanha(str(chat.id))
    chat.send('Campaign ok!')


@bot.command('stats')
def character_stat_full(chat, message, args):
    """Show character's stats (Eg.: /stats [ @user ] )"""
    username = ''
    if args:
        username = args[0].replace('@','')
    if username:
        # usuario sendo informado
        nome_usuario = username
        personagem = stores.procurar_personagem(str(chat.id), nome_usuario)
    else:
        nome_usuario = message.sender.username
        personagem = stores.procurar_personagem(str(chat.id), nome_usuario)
    if not personagem:
        chat.send('Character not found.')
        return
    stats = {
        'name': personagem.nome,
        'rank': constants.rankLabels[int(personagem.rank)],
        'reputation': personagem.reputation,
        'specialization': personagem.especialization,
        'fa': personagem.fa,
        'nfa': personagem.nfa,
        'hp': constants.hpLabels[personagem.hp],
        'armor': constants.used_values[personagem.armor],
        'drugs': constants.used_values[personagem.drugs],
        'kills': personagem.kills,
        'totalKills': personagem.totalKills
    }
    description = """
Name: {name}
Rank: {rank}
Reputation: {reputation}
Specialization: {specialization}
FA: {fa}
NFA: {nfa}

HP: {hp}
Armor: {armor}
Drugs: {drugs}

Kills: {kills}
Total Kills: {totalKills}


    """.format(**stats)
    chat.send(description)


@bot.command('stat')
def set_character_stat(chat, message, args):
    """/stat [@user] stat new_value"""
    if not args:
        chat.send('Incorrect syntax')
        chat.send('/help stat')
        return
    com_username = '@' in args[0]
    if com_username:
        # usuario sendo informado
        nome_usuario = args[0].replace('@', '')
        personagem = stores.procurar_personagem(str(chat.id), nome_usuario)
    else:
        nome_usuario = message.sender.username
        personagem = stores.procurar_personagem(str(chat.id), nome_usuario)
    if not personagem:
        chat.send('Character not found.')
    stat = args[1 if com_username else 0]
    atributos = [
        'name', 'rank', 'specialization', 'reputation',
        'fa', 'nfa', 'hp', 'armor', 'drugs', 'kills', 'totalKills'
    ]
    if stat not in atributos:
        chat.send('Stat not known')
        return
    novo_valor = " ".join(args[2 if com_username else 1:])
    if not novo_valor:
        # sem valor, entao mostrar valor e valores possiveis
        chat.send(details.detail(stat, personagem))
        return
    if details.validate_value(stat, novo_valor):
        stores.mudar_atributo(str(chat.id), nome_usuario, stat, novo_valor)
        chat.send('Character changed.')
    else:
        chat.send('Invalid value.')


@bot.command('damage')
def causar_dano(chat, message, args):
    """Damage a character. (Eg.: /damage [ @user ] )"""
    if args:
        # usuario sendo informado
        nome_usuario = args[0].replace('@', '')
        personagem = stores.procurar_personagem(str(chat.id), nome_usuario)
    else:
        nome_usuario = message.sender.username
        personagem = stores.procurar_personagem(str(chat.id), nome_usuario)
    if not personagem:
        chat.send('Character not found.')
        return
    hp = personagem.hp
    if hp == 0:
        chat.send('Character already is dead.')
        return
    hp = hp - 1
    stores.save_hp_character(personagem.id, hp)
    chat.send('Character is ' + constants.hpLabels[hp])


@bot.command('kills')
def listar_mortes(chat, message, args):
    """Total mission deaths"""
    # obter campanha do chat
    if not stores.procurar_campanha_ativa(str(chat.id)):
        chat.send('Campaign not found.')
        return
    # obter personagens da campanha do chat
    personagens = stores.procurar_personagens(str(chat.id))
    # listar o nome dos PJs e as respectivas mortes
    lista = ['Troopers', '-------------']
    for p in personagens:
        lista.append('{rank} {nome} {kills} (@{player})'.format(
            rank=constants.rankLabels[int(p.rank)],
            nome=p.nome,
            kills=p.kills,
            player=p.usuario))
    lista.append('-------------')
    chat.send('\n'.join(lista))

@bot.command('kill')
def incrementar_mortes(chat, message, args):
    """Increases the deaths caused by the character. (ex.: /kill 3)"""
    # obter campanha do chat
    if not stores.procurar_campanha_ativa(str(chat.id)):
        chat.send('Campaign not found.')
        return
    if args:
        print(args)
        if args[0].isdigit():
            stores.somar_mortes(
                str(chat.id), message.sender.username, int(args[0]))
            chat.send('Character changed.')
        else:
            chat.send('Incorrect syntax. Eg.: /kill 4')
    else:
        chat.send('Incorrect syntax. Eg.: /kill 44')

@bot.command('delpjs')
def apagar_pjs(chat, message, args):
    texto = retirar_nome_bot(message.text)
    nome_campanha = " ".join(texto.split()[1:])
    stores.apagar_pjs_campanha(nome_campanha)
    chat.send('Characters deleted.')


@bot.command('newitem')
def new_item(chat, message, args):
    """Trooper gets a item (ex.: /newitem Escova)"""
    texto = retirar_nome_bot(message.text)
    # obter campanha do chat
    if not stores.procurar_campanha_ativa(str(chat.id)):
        chat.send('Campaign not found.')
        return
    params = texto.split()[1:]
    if len(params) == 0:
        chat.send('Incorrect syntax. Eg.: /newitem Escova')
        return
    nome = " ".join(params)
    print(params)
    print(nome)
    item = stores.novo_item(
        str(chat.id),
        message.sender.username,
        nome
    )
    if item:
        chat.send('New item added.')
    else:
        chat.send('Campaign or player not found.')


@bot.command('listitems')
def list_items(chat, message, args):
    """List items. (ex.: /listitems [ @user ] )"""
    texto = retirar_nome_bot(message.text)
    if not stores.procurar_campanha_ativa(str(chat.id)):
        chat.send('Campaign not found.')
        return
    com_username = '@' in texto
    if com_username:
        # usuario sendo informado
        nome_usuario = texto.split()[1].replace('@', '')
    else:
        nome_usuario = message.sender.username
    personagem = stores.procurar_personagem(str(chat.id), nome_usuario)
    if not personagem:
        chat.send('Character not found.')
        return
    lista = stores.procurar_itens(personagem)
    itens = []
    if not lista:
        chat.send('No items')
        return
    for i in lista:
        itens.append('{nome}'.format(nome=i.nome))
    chat.send('\n'.join(itens))

@bot.command('quemsou')
def quem_sou(chat, message, args):
    chat.send('Eu sou o usuário {}'.format(message.sender.id))

@bot.command('games')
def list_games(chat, message, args):
    games = stores.procurar_campanhas_ativas()
    lista = ['Games', '-------------']
    for g in games:
        lista.append('{nome} {data_criacao} {status})'.format(
            nome=g.nome,
            data_criacao=g.data_criacao,
            status=g.ativa))
    lista.append('-------------')
    chat.send('\n'.join(lista))    